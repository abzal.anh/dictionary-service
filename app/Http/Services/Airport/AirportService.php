<?php

namespace App\Http\Services\Airport;

use stdClass;

class AirportService implements AirportServiceInterface
{
    /**
     * @return stdClass
     */
    public function getAll(): stdClass
    {
        $url = config('app.hrefs.airports');
        $options = [
            "http" => [
                "header" => "User-Agent: request"
            ]
        ];
        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);
        $data = json_decode($response, true);

        return json_decode(file_get_contents($data['download_url']));
    }

    /**
     * @param stdClass $airports
     * @return array
     */
    public function filter(stdClass $airports): array
    {
        $airportsNames = [];

        foreach ($airports as $airport) {
            if (isset($airport->airportName)) {
                $airportsNames[] = $airport->airportName->ru;
                $airportsNames[] = $airport->airportName->en;
            }
        }

        return array_filter($airportsNames);
    }
}
