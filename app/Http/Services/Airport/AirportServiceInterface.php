<?php

namespace App\Http\Services\Airport;

use stdClass;

interface AirportServiceInterface
{
    /**
     * @return mixed
     */
    public function getAll(): stdClass;

    public function filter(stdClass $airports): array;
}
