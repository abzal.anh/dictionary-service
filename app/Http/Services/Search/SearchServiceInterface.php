<?php

namespace App\Http\Services\Search;

use App\DataObjects\Airports\AirportsRequestDto;
use stdClass;

interface SearchServiceInterface
{
    /**
     * @param array $data
     * @param AirportsRequestDto $query
     * @return array
     */
    public function autocomplete(array $data, AirportsRequestDto $query): array;
}
