<?php

namespace App\Http\Services\Search;

use App\DataObjects\Airports\AirportsRequestDto;

class SearchService implements SearchServiceInterface
{
    /**
     * @param array $data
     * @param AirportsRequestDto $query
     * @return array
     */
    public function autocomplete(array $data, AirportsRequestDto $query): array
    {
        $result = [];
        foreach ($data as $value) {
            if (stripos($value, $query->getName()) === 0) {
                $result[] = $value;
            }
        }

        return $result;
    }
}
