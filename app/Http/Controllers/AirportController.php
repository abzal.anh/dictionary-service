<?php

namespace App\Http\Controllers;

use App\Factories\Airports\AirportsRequestFactory;
use App\Http\Requests\AirportSearchRequest;
use App\Http\Services\Airport\AirportServiceInterface;
use App\Http\Services\Search\SearchServiceInterface;
use Illuminate\Http\JsonResponse;

class AirportController
{
    private AirportServiceInterface $service;
    private SearchServiceInterface $searchService;

    public function __construct(AirportServiceInterface $service, SearchServiceInterface $searchService)
    {
        $this->service = $service;
        $this->searchService = $searchService;
    }

    /**
     * @param AirportSearchRequest $request
     * @return JsonResponse
     */
    public function searchByNameAutocomplete(AirportSearchRequest $request): JsonResponse
    {
        $requestData = $request->validated();
        $name = AirportsRequestFactory::buildAirportsRequestDto($requestData);
        $airports = $this->service->getAll();
        $filtered = $this->service->filter($airports);
        $result = $this->searchService->autocomplete($filtered, $name);

        return response()->json($result);
    }
}
