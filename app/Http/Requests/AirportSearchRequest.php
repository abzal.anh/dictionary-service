<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AirportSearchRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            "name" => 'nullable|string',
        ];
    }
}
