<?php

namespace App\Factories\Airports;

use App\DataObjects\Airports\AirportsRequestDto;

class AirportsRequestFactory
{
    public static function buildAirportsRequestDto(array $data): AirportsRequestDto
    {
        return (new AirportsRequestDto())
            ->setName($data['name'] ?? null);
    }
}
