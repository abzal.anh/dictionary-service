<?php

namespace App\Providers;

use App\Http\Services\Airport\AirportService;
use App\Http\Services\Airport\AirportServiceInterface;
use App\Http\Services\Search\SearchService;
use App\Http\Services\Search\SearchServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AirportServiceInterface::class, AirportService::class);
        $this->app->bind(SearchServiceInterface::class, SearchService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
